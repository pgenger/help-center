import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterListComponent } from './master-list/master-list.component';
import { DetailComponent } from './detail/detail.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: MasterListComponent
    },
    {
        path: 'article/:id',
        component: DetailComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
