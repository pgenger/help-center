import { HeapCenterPage } from './app.po';

describe('heap-center App', function() {
  let page: HeapCenterPage;

  beforeEach(() => {
    page = new HeapCenterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
